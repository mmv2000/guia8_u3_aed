#include <iostream>
#include <chrono>
#include "Metodos.h"

using namespace std;

Metodos::Metodos() {
}

// Imprime arreglo
void Metodos::Imprimir(int *arreglo, int tam){

  for(int i = 0; i < tam; i++){
    cout << " " << arreglo[i] << "|";
  }
  cout << "\n" << endl;
}

// Imprime arreglo ordenado por el metodo
void Metodos::Arr_seleccion(int* arreglo, int tam){
  int arrAux;
  int menor;
  int k;

  double secs;

  auto inicio = std::chrono::high_resolution_clock::now();

  for(int i = 0; i < tam; i++){
    menor = arreglo[i];
    k = i;

    for(int j = i+1; j < tam; j++){
      if(arreglo[j] < menor){
        menor = arreglo[j];
        k = j;
      }
    }
    arreglo[k] = arreglo[i];
    arreglo[i] = menor;
  }

  cout << "Metodo Selección" << endl;
  Imprimir(arreglo, tam);
}

// Imprime tiempo del metodo
void Metodos::Seleccion(int* arreglo, int tam){
  int arrAux;
  int menor;
  int k;

  // tiempo de inicio del metodo
  auto inicio = std::chrono::high_resolution_clock::now();

  for(int i = 0; i < tam; i++){
    menor = arreglo[i];
    k = i;

    // Parte de una pos más a la derecha
    for(int j = i+1; j < tam; j++){
      // Busqueda del numero más pequeño
      if(arreglo[j] < menor){
        menor = arreglo[j];
        k = j;
      }
    }
    // Se ajustan las posiciones de los número, menores más a la izquierda
    arreglo[k] = arreglo[i];
    arreglo[i] = menor;
  }
  // Tiempo final del metodo
  auto fin = std::chrono::high_resolution_clock::now();

  // se calcula el tiempo del metodo en segundos y lo transforma a milisegundos
  std::chrono::duration <double> transcurrido = fin - inicio;
  std::chrono::milliseconds mili = std::chrono::duration_cast< std::chrono::milliseconds > (transcurrido);

  // cout << "tiempo:     " << transcurrido.count() << endl;
  cout << " Seleccion: " << mili.count() << " Milisegundos" << endl;
}

// Imprime arreglo ordenado por el metodo
void Metodos::Arr_quicksort(int* arreglo, int tam){
  int pilamenor[tam];
  int pilamayor[tam];
  int tope, ini, fin, pos;
  int izq, der, aux;
  bool band;

  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = tam - 1;

  auto inicio = std::chrono::high_resolution_clock::now();

  while (tope >= 0) {
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    tope = tope - 1;

    // Reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = true;

    while (band == true) {
      while((arreglo[pos] <= arreglo[der]) && (pos != der)){
        der = der - 1;
      }
      if (pos == der) {
        band = false;
      }
      else{
        aux = arreglo[pos];
        arreglo[pos] = arreglo[der];
        arreglo[der] = aux;
        pos = der;

        while ((arreglo[pos] >= arreglo[izq]) && (pos != izq)) {
          izq = izq + 1;
        }

        if (pos == izq) {
          band = false;
        }
        else{
          aux = arreglo[pos];
          arreglo[pos] = arreglo[izq];
          arreglo[izq] = aux;
          pos = izq;
        }
      }
    }

    if (ini < (pos - 1)) {
      tope = tope + 1;
      pilamenor[tope] = ini;
      pilamayor[tope] = pos - 1;
    }
    if (fin > (pos + 1)) {
      tope = tope + 1;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }

  cout << "Metodo Quicksort" << endl;
  Imprimir(arreglo, tam);
}


// Imprime tiempo del metodo
void Metodos::Quicksort(int* arreglo, int tam){
  int pilamenor[tam];
  int pilamayor[tam];
  int tope, ini, fin, pos;
  int izq, der, aux;
  bool band;

  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = tam - 1;

  auto inicio = std::chrono::high_resolution_clock::now();

  while (tope >= 0) {
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    tope = tope - 1;

    // Reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = true;

    while (band == true) {
      while((arreglo[pos] <= arreglo[der]) && (pos != der)){
        der = der - 1;
      }
      if (pos == der) {
        band = false;
      }
      else{
        aux = arreglo[pos];
        arreglo[pos] = arreglo[der];
        arreglo[der] = aux;
        pos = der;

        while ((arreglo[pos] >= arreglo[izq]) && (pos != izq)) {
          izq = izq + 1;
        }

        if (pos == izq) {
          band = false;
        }
        else{
          aux = arreglo[pos];
          arreglo[pos] = arreglo[izq];
          arreglo[izq] = aux;
          pos = izq;
        }
      }
    }

    if (ini < (pos - 1)) {
      tope = tope + 1;
      pilamenor[tope] = ini;
      pilamayor[tope] = pos - 1;
    }
    if (fin > (pos + 1)) {
      tope = tope + 1;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }

  auto final = std::chrono::high_resolution_clock::now();

  std::chrono::duration <double> transcurrido = final - inicio;
  std::chrono::milliseconds mili = std::chrono::duration_cast< std::chrono::milliseconds > (transcurrido);

  cout << " Quicksort: " << mili.count() << " Milisegundos" << endl;

}
