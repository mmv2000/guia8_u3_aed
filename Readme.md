# Metodos de Ordenamiento
Previamente se entregaron dos código en lenguaje C y se solicitó traspasarlo a C++ en función de mantener una estructura para
la comparación del tiempo de procesamiento y aplicación de 2 métodos de ordenamiento, los códigos consistían en 2 métodos de ordenamiento
método de selección y metodo quicksort, los cuales manipulaban un arrreglo con datos generados aleatoriamente y que calculaban el tiempo
de ejecución de cada método respectivamente. Se solicita transcribir los códigos previos escritos en C en lenguaje C++ con la utilización 
de clases y métodos necesarios para que cumpla el obejtivo de ordenar un arreglo con números aleatorios de forma ascendente mostrando el
tiempo en milisegundos.

# Como funciona
El código solicita parámetros previos por terminal estos son caracteres que luego serán los parámetros a tener en cuenta por el programa
para desarrollar las líneas de código. Los parámetros consisten en ingresar un número entre 0 y 1 millón y posteriormente un caracter s o n
que indicará al programa si ver los arreglos y tiempos (s) o bien solo tiempos (n). Al momento de ingresar estos parámetros el programa 
genera un arreglo con números aleatorios el cual será modificado por funciones que contienen los métodos previamente mencionados, 
mostrando finalmente por pantalla los tiempos y/o arreglos creados según corresponda con los parámetros ingresados.

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir la terminal en la carpeta donde
se almacenaron los archivos. Prontamente se debe ejecutar el comando make en la terminal para la respectiva compilación 
de los archivos y luego ejecutar el comando ./programa || caracter(número mayor que 0 y menor a 1 millón) || caracter "s" o "n"
para iniciar el programa y su interacción. Ejemplo de ejecución: ./programa 1000 s. En este ejemplo se inciará le program con un arreglo
de tamaño 1000 con números aleatorios entre 0 y 1000 y  se mostrará el contenido de los arreglos y sus respectivos tiempos en cada método. 

# Construido con 

- sistema operativo (SO): Ubuntu

- lenguaje de programación: C++

- libreria(s): iostream, fstream, cstdlib, stdio.h, string.h, chrono

- editor de texto: Atom

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.52.0

# Autor

- Martín Muñoz Vera 
