#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Metodos{

  private:

  public:
    Metodos();
    void Seleccion(int* arreglo, int tam);
    void Arr_seleccion(int* arreglo, int tam);
    void Imprimir(int* arreglo, int tam);
    void Quicksort(int* arreglo, int tam);
    void Arr_quicksort(int* arreglo, int tam);
};
#endif
