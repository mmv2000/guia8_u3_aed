// Librerias
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <chrono>
// Define
#include "Metodos.h"

using namespace std;

// Funcion que muestra vector original
void Imprimir_vector(int* arreglo, int tam){
  cout << "Arreglo Original: " << endl;

  for(int i = 0; i < tam; i++){
    cout << " " << arreglo[i] << "|";
  }
  cout << "\n" << endl;
}

// Funcion que muestra los tiempos de los metodos
void Mostrar_tiempos(int *arrAux1, int *arrAux2, int tam, Metodos metodos){
  cout << "Tiempo para ordenar de cada método:" << endl;

  metodos.Seleccion(arrAux1, tam);
  metodos.Quicksort(arrAux2, tam);
}

// Funcion que genera el vector y añade valores aleatorios
int crearArreglo(int tam, char *argv[]) {
  int elemento;
  Metodos metodos = Metodos();
  string VER = argv[2];

  // Valida que el tamaño sea un entero mayor que 0 y menor a i millon
  if(tam > 0 && tam < 1000000) {
    int arrAux[tam];
    int arrSeleccion[tam];
    int arrQuicksort[tam];

    // Rellena arreglo con numeros aleatorios
    for (int i = 0; i < tam; i++){
      arrAux[i] = (rand() % tam) + 1;
    }

    // Arreglos iguales al original para manipular los datos
    for (int j = 0; j < tam; j++){
      elemento = arrAux[j];
      arrSeleccion[j] = elemento;
      arrQuicksort[j] = elemento;
    }

    // Valida el segundo parametro, si es un caracter "s" muestra arreglos y tiempos
    if (VER == "s") {
      Imprimir_vector(arrAux, tam);
      cout << "Arreglos Ordenados" << endl;
      metodos.Arr_seleccion(arrSeleccion, tam);
      metodos.Arr_quicksort(arrSeleccion, tam);
      Mostrar_tiempos(arrSeleccion, arrQuicksort, tam, metodos);
      //metodos.Quicksort(arrQuicksort, tam);
    }

    // Si es un caracter "n" muestra solo tiempos de los metodos
    else if(VER == "n"){
      Mostrar_tiempos(arrSeleccion, arrQuicksort, tam, metodos);
    }

    // si no se ingresa un segundo parametro, muestra mensaje de adevertencia
    // Se cierra el programa pidiendo que se vuelva a iniciar
    else {
      cout << "Debe ingresar un parametro s o n" << endl;
      cout << "s para ver el arreglo y tiempo\n y n solo para tiempo " << '\n';
      return -1;
    }
  }

  else{
    cout << "Debe ingresar un numero mayor a 0 y menor a 1000000" << endl;
    return -1;
  }
}

// Valida parametro de tamaño del arreglo
int Parametros(int argc, char*argv[]) {
  // Cambia el caracter recibido a un número, correspondiente al tamaño
  if (argc == 3) {
    int tam = stoi(argv[1]);
    crearArreglo(tam, argv);
  }

  else{
    cout << "No ha ingresado suficientes parámetros" << endl;
    cout << "Vuelva a reiniciar el programa" << endl;
  }
}

// Main
int main(int argc, char *argv[]) {
  int entrada;
  // Valida ingreso de parametros
  entrada = Parametros(argc, argv);
  return 0;
}
